/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Modelo.ListaNumeros;
import Negocio.Matriz_Numeros;
import java.util.Scanner;

/**
 *
 * @author 1152047 - Fredy Peñaranda
 */
public class Prueba3 {

    public static void main(String[] args) {

        //Pedir Datos
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a String");
        String vDist0 = sc.nextLine();
        System.out.println("");

        //Separar por ; y ,
        String[] datoPUNTOCOMA = vDist0.split(";");
        String[] aux = datoPUNTOCOMA[0].split(",");

        //Crear Matriz Vacia
        int filas = datoPUNTOCOMA.length; //3
        int columnas = aux.length; //4
        Matriz_Numeros matriz = new Matriz_Numeros(filas, columnas);

        //Crear ListaNum para enviar a Matriz
        ListaNumeros listaNum = new ListaNumeros(columnas);
        ListaNumeros[] vector = new ListaNumeros[filas];

        //Ciclos para dividir vDivist
        int x = 0;
        
        /**
         * @param x = Va a controlar las filas[3]
         */
        while (x<filas) {

            String[] datoCOMA = datoPUNTOCOMA[x].split(",");

            /**
             * @param y = Va a controlar las columnas[4]
             */
            for (int y = 0; y < datoCOMA.length; y++) {
                //1,2,3,4;5,6,7,8;9,10,11,12
                float nuevoNum = Float.parseFloat(datoCOMA[y]);
                System.out.println("[" + x + "]" + "[" + y + "]:" + "\t----" + nuevoNum + "----");
                listaNum.adicionar(y, nuevoNum);
            }
            
            System.out.println("LISTA: " + listaNum.toString() + "\n");
            matriz.adicionarVector(x, listaNum);
            System.out.println("\n"+"Primera Matriz: " + "\n" + matriz.toString());
            x++;
        }//CICLO WHILE
        
        System.out.println("MATRIZ----" + listaNum.toString());
        System.out.println("\n"+"Primera Matriz: " + "\n" + matriz.toString());

    }

}
