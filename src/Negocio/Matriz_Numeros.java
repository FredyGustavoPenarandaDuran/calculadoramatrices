/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.ListaNumeros;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author madarme
 */
public class Matriz_Numeros {

    private ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    /**
     * Puedo utilizarlo para matrices dispersa
     *
     * @param cantFilas
     */
    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas
     * @param cantColumnas
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }

    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    public ListaNumeros getFila(int i) {
        return this.filas[i];
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void prueba() {
        ListaNumeros lista = new ListaNumeros(4);
        lista.adicionar(0, 1);
        lista.adicionar(1, 2);
        lista.adicionar(2, 3);
        lista.adicionar(3, 4);
        adicionarVector(1, lista);
        ListaNumeros lista2 = new ListaNumeros(4);
        lista2.adicionar(0, 12);
        lista2.adicionar(1, 22);
        lista2.adicionar(2, 32);
        lista2.adicionar(3, 42);
        adicionarVector(2, lista2);
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    /**
     * Esté mètodo funciona SI Y SOLO SI LA MATRIZ ESTÁ CREADA CON FILAS Y
     * COLUMNAS
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param nuevoDato dato a ingresar en i,j
     */
    public void setElemento(int i, int j, float nuevoDato) {

    }

    @Override
    public String toString() {
        String msg = "";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    /**
     *
     * 🅼🅴🆃🅾🅳🅾🆂 🅳🅴 🅻🅰 🅰🅲🆃🅸🆅🅸🅳🅰🅳
     */
    /**
     * Metodo que suma la matriz, con una segunda matriz. Retorna la matriz
     * resultante.
     *
     * @return Matriz_Resultado matriz resultante de sumar this.matriz con la
     * matriz2.
     */
    public Matriz_Numeros getSuma() {

        return null;
    }

    public Matriz_Numeros getResta() {

        return null;
    }

    public Matriz_Numeros getMultiplicacion() {

        return null;
    }

    public void getTranspuesta() {
    }
}
